#!/usr/bin/python3
import os
import sys
import tarfile
import tempfile
import argparse
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler, Packages

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to capture a build workspace for later use.')
parser.add_argument('--environment', type=str, required=True)
arguments = parser.parse_args()

# Create a temporary file, then open the file as a tar archive for writing
# We don't want it to be deleted as storePackage will move the archive into it's cache
archiveFile = tempfile.NamedTemporaryFile(delete=False)
archive = tarfile.open( fileobj=archiveFile, mode='w' )

# Now determine the path we should be archiving
# We assume we are being run from the root of the Workspace, so the current working directory is good enough for our purposes here
pathToArchive = os.getcwd()

# Add all the files which need to be in the archive into the archive
# We want to capture the tree as it is inside the install directory and don't want any trailing slashes in the archive as this isn't standards compliant
# Therefore we list everything in the install directory and add each of those to the archive, rather than adding the whole install directory
filesToInclude = os.listdir( pathToArchive )
for filename in filesToInclude:
	fullPath = os.path.join(pathToArchive, filename)
	archive.add( fullPath, arcname=filename, recursive=True )

# Close the archive, which will write it out to disk, finishing what we need to do here
# This is also necessary on Windows to allow for storePackage to move it to it's final home
archive.close()
archiveFile.close()

# Initialize the archive manager
ourArchive = Packages.Archive( arguments.environment, 'Workspaces', usingCache = False )

# Determine which SCM revision we are storing
# This will be embedded into the package metadata which might help someone doing some debugging
# GIT_COMMIT is set by Jenkins Git plugin, so we can rely on that for most of our builds
scmRevision = ''
if os.getenv('GIT_COMMIT') != '':
	scmRevision = os.getenv('GIT_COMMIT')

# Determine the package name
# For this we take the job name and transform spaces into underscores
package = os.getenv('JOB_NAME').replace(' ', '_')

# Add the package to the archive
ourArchive.storePackage( package, archiveFile.name, scmRevision )

# All done!
sys.exit(0)
